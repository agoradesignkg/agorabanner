<?php

namespace Drupal\agorabanner\Plugin\Block;

use Drupal\agorabanner\BannerfinderInterface;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityRepository;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides agorabanner block.
 *
 * @Block(
 *   id = "agorabanner_block",
 *   admin_label = @Translation("Agorabanner Block"),
 *   category = @Translation("Agorabanner")
 * )
 */
class Agorabanner extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The banner finder.
   *
   * @var \Drupal\agorabanner\BannerfinderInterface
   */
  protected $bannerfinder;

  /**
   * The entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * The entity repository.
   *
   * @var \Drupal\Core\Entity\EntityRepository
   */
  protected $entityRepository;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a PromotedContent object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\agorabanner\BannerfinderInterface $bannerfinder
   *   The banner finder.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository.
   * @param \Drupal\Core\Entity\EntityRepository $entity_repository
   *   The entity repository.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, BannerfinderInterface $bannerfinder, EntityDisplayRepositoryInterface $entity_display_repository, EntityRepository $entity_repository, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->bannerfinder = $bannerfinder;
    $this->entityDisplayRepository = $entity_display_repository;
    $this->entityRepository = $entity_repository;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('agorabanner.bannerfinder'),
      $container->get('entity_display.repository'),
      $container->get('entity.repository'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'view_mode' => 'default',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['view_mode'] = [
      '#type' => 'select',
      '#title' => $this->t('View mode'),
      '#description' => $this->t('The view mode that will be used for rendering the banner in the block.'),
      '#default_value' => $this->configuration['view_mode'],
      '#options' => $this->getAvailableViewModes(),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['view_mode'] = $form_state->getValue('view_mode');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $items = $this->loadBannerEntitiesFromActiveNode();
    $output = [];
    if (!empty($items)) {
      $output['#theme'] = 'agorabanner_slider';
      $output['#items'] = $items;
    }
    return $output;
  }

  /**
   * @inheritDoc
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), ['route']);
  }

  /**
   * @inheritDoc
   */
  public function getCacheTags() {
    $tags = parent::getCacheTags();
    $node = $this->bannerfinder->getActiveNode();
    if (!empty($node)) {
      $tags = Cache::mergeTags($tags, ['node:' . $node->id()]);
    }
    return $tags;
  }

  /**
   * Loads banner entities as renderable array from active node.
   *
   * @return array
   *   A renderable array of banner entities (or empty).
   */
  protected function loadBannerEntitiesFromActiveNode() {
    $bannerEntities = [];
    $ids = $this->bannerfinder->getBannerIdsFromActiveNode();
    foreach ($ids as $id) {
      /** @var \Drupal\agorawidget\AgorawidgetInterface $banner_entity */
      $banner_entity = $this->entityTypeManager
        ->getStorage('agorawidget')
        ->load($id);
      $banner_entity = $this->entityRepository
        ->getTranslationFromContext($banner_entity);
      $view_builder = $this->entityTypeManager->getViewBuilder('agorawidget');
      $banner_renderable = $view_builder->view($banner_entity, $this->configuration['view_mode']);
      $bannerEntities[] = $banner_renderable;
    }
    return $bannerEntities;
  }

  /**
   * Gets available view modes of banner entities for block form configuration.
   */
  protected function getAvailableViewModes() {
    $options = [
      // Always add the 'default' view mode.
      'default' => 'Default',
    ];
    $form_modes = $this->entityDisplayRepository->getViewModes('agorawidget');
    foreach ($form_modes as $id => $info) {
      $options[$id] = $info['label'];
    }
    return $options;
  }

}
